#pragma once
#include "DetectBubbleDll.h"
#include "DetectBubble.h"
//#define DEBUG_MEMCPY

//int findBubble(char* p_A, char* p_C, char* p_G, char* p_T, const int rows, const int cols)
//{
//	char* p_in[4] = { p_A,p_C,p_G,p_T };
//	using cv::Mat;
//	try
//	{
//		std::vector<Mat> images;
//		for (size_t i = 0; i < 4; i++)
//		{
//			Mat image = Mat(rows, cols, CV_16U, cv::Scalar(0));
//			unsigned short* p = (unsigned short*)p_in[i];
//			memcpy(image.data, p, sizeof(unsigned short)*rows*cols);
//#ifdef DEBUG_MEMCPY
//			cv::imwrite("./img.tif", image);
//#endif
//			images.push_back(image);
//		}
//		DetectBubble db;
//		return db.detect(images);
//	}
//	catch (const std::exception&)
//	{
//		return -3;
//	}
//}

int findBubble(char* p_A, char* p_C, char* p_G, char* p_T, const int rows, const int cols)
{
	char* p_in[4] = { p_A,p_C,p_G,p_T };
	using cv::Mat;
	using std::string;
	try
	{
		std::vector<Mat> images;
		for (size_t i = 0; i < 4; i++)
		{
			Mat image = cv::imread(string(p_in[i]), CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			if (image.empty() || image.rows != rows || image.cols != cols)
			{
				return -101;
			}
			images.push_back(image);
		}
		DetectBubble db;
		return db.detect(images);
	}
	catch (const std::exception&)
	{
		return -3;
	}
}


int findBubble(const std::vector<std::string> fns)
{
	using cv::Mat;
	try
	{
		std::vector<Mat> images;
		for (size_t i = 0; i < 4; i++)
		{
			Mat image = cv::imread(fns[i], CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			if (image.empty())
			{
				return -101;
			}
			images.push_back(image);
		}
		DetectBubble db;
		return db.detect(images);
	}
	catch (const std::exception&)
	{
		return -3;
	}
}
