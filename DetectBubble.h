#ifndef DETECT_BUBBLE_H
#define DETECT_BUBBLE_H

#include <opencv2/opencv.hpp>
#include <vector>
#include<iostream>
#include<map>


class DetectBubble
{
public:
	DetectBubble();						
	int detect(const std::vector<cv::Mat>& images);
	~DetectBubble();
private:
	struct Params
	{
		bool filterByColor;
		float maxBlobColor;

		bool filterByArea;
		float minArea, maxArea;

		bool filterByCircularity;
		float minCircularity, maxCircularity;

		bool filterByInertia;
		float minInertiaRatio, maxInertiaRatio;

		bool filterByConvexity;
		float minConvexity, maxConvexity;

		bool filterByRadius;
		float minRadius;
	};
	Params params;
	cv::Point2i start;

	void morphProcess(const cv::Mat& bw1, const cv::Mat& bw2, cv::Mat& bw_border);
	int differAndBw(const cv::Mat& img1, const cv::Mat& img2, cv::Mat& bw);

	struct Center
	{
		cv::Point2d location;
		double radius;
		double confidence;
	};
	void findBlobs(const cv::Mat& img, const cv::Mat& binaryImage, std::vector<Center>& centers);

	void checkborder(const cv::Mat& bw, cv::Mat& bw_cutborder);
};



#endif