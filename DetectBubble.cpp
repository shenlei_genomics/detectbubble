#include "DetectBubble.h"

//#define DEBUG_DETCT_BUBBLE
//#define _DETCT_BUBBLE_DEBUG
//#define DEBUG_BLOB_DETECTOR

DetectBubble::DetectBubble()
{
	this->params.filterByArea = true;
	this->params.minArea = 800;
	this->params.maxArea = std::numeric_limits<float>::max();
	//filter by shape (circulartiy)
	this->params.filterByCircularity = true;
	this->params.minCircularity = 0.27;
	this->params.maxCircularity = std::numeric_limits<float>::max();
	// filter by convex
	this->params.filterByConvexity = true;
	this->params.minConvexity = 0.9;
	this->params.maxConvexity = std::numeric_limits<float>::max();

	this->params.filterByColor = true;
	this->params.maxBlobColor = 10000;
	//filter by inertia
	this->params.filterByInertia = false;
	this->params.minInertiaRatio = 0.2f;
	this->params.maxInertiaRatio = std::numeric_limits<float>::max();
	//filter by radius
	this->params.filterByRadius = true;
	this->params.minRadius = 19;

	this->start = cv::Point2i(0, 0);
}		

int DetectBubble::detect(const std::vector<cv::Mat>& images)
{
	try
	{
		using cv::Mat;
		using cv::Size;
		using std::vector;
		if (images.size() < 2 )
		{			
			return -2;
		}				
		const int rows = images[0].rows;
		const int cols = images[0].cols;
		for (size_t i = 1; i < images.size(); i++)
		{
			if (images[i].cols != cols|| images[i].rows != rows)
			{				
				return -21;
			}
		}
		Mat bw1, bw2;
		if (images.size() == 2)
		{
			bw2 = Mat::zeros(rows, cols, CV_8U);
			differAndBw(images[0], images[1], bw1);
		}
		else
		{
			if (images.size() < 4)
			{				
				return -22;						
			}
			//assume images: index 0 - channel A, index 1 - channel C, index 2 - channel G, index 3 - channel T
			if (differAndBw(images[0], images[2], bw1) < 0 || differAndBw(images[1], images[3], bw2) < 0)
			{				
				return -11;
			}
		}
		Mat bw_border;
		morphProcess(bw1, bw2, bw_border);
		if (bw_border.cols < 20 || bw_border.rows < 20)
		{
			return -222;
		}
		//set bubble's max area to be half of bw_border's area.
		params.maxArea = bw_border.cols * bw_border.rows / 2;
		vector<Center> centers;
		findBlobs(images[0], bw_border, centers);				
		return centers.size();				
	}
	catch (const std::exception& e)
	{		
		return -111;
	}			
}

DetectBubble::~DetectBubble()
{
}

void DetectBubble::morphProcess(const cv::Mat & bw1, const cv::Mat & bw2, cv::Mat & bw_border)
{
	using cv::Mat;
	Mat bw = bw1 | bw2;
	int erosion_type = cv::MORPH_ELLIPSE;
	int erosion_size = 7;
	Mat element = cv::getStructuringElement(erosion_type,
		cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		cv::Point(erosion_size, erosion_size));

	//Mat bw_cutborder;
	//checkborder(bw, bw_cutborder);
	//cv::dilate(bw_cutborder, bw, element);	
	cv::dilate(bw, bw, element);
#ifdef _DETCT_BUBBLE_DEBUG
	imwrite("./dilate.tif", bw);
#endif	
	cv::erode(bw, bw, element);
	cv::erode(bw, bw, element);
#ifdef _DETCT_BUBBLE_DEBUG
	imwrite("./erode.tif", bw);
#endif	
	//check boarder intensity.
	int border_val = 0;
	int y = 0;
	int border_sum = 0;
	for (size_t x = 0; x < bw.cols; x++)
	{
		border_sum += bw.at<unsigned char>(y, x);
	}
	y = bw.rows - 1;
	for (size_t x = 0; x < bw.cols; x++)
	{
		border_sum += bw.at<unsigned char>(y, x);
	}
	for (size_t r = 1; r < bw.rows - 1; r++)
	{
		border_sum += bw.at<unsigned char>(r, 0) + bw.at<unsigned char>(r, bw.cols - 1);
	}
	if (border_sum / 255 >(bw.cols + bw.rows))
	{
		border_val = 255;
	}

	int border_width = 5;
	cv::copyMakeBorder(bw, bw_border, border_width, border_width, border_width, border_width, cv::BORDER_CONSTANT, border_val);
#ifdef _DETCT_BUBBLE_DEBUG
	imwrite("./Border.tif", bw_border);
#endif
}

int DetectBubble::differAndBw(const cv::Mat & img1, const cv::Mat & img2, cv::Mat & bw)
{
	using cv::Mat;
	using cv::Size;
	//step1 : calculate mean(img1) and mean(img2), calculate illum_coef
	Mat tmp_m, tmp_sd;
	cv::meanStdDev(img1, tmp_m, tmp_sd);
	double m1 = tmp_m.at<double>(0, 0);
	cv::meanStdDev(img2, tmp_m, tmp_sd);
	double m2 = tmp_m.at<double>(0, 0);
	//for 16bits image, set 150 to be threshold as no signal image. 
	//so for changguang 12bit image, this must be modified.
	if (m1 < 150 || m2 < 150)
		return -1;
	double illum_coef = m1 / m2;
	//differ
	Mat d;
	Mat img1_32F, img2_32F;
	img1.convertTo(img1_32F, CV_32F);
	img2.convertTo(img2_32F, CV_32F);
	d = cv::abs(img1_32F - illum_coef*img2_32F);
	cv::meanStdDev(d, tmp_m, tmp_sd);
	double m = tmp_m.at<double>(0, 0);
	double std = tmp_sd.at<double>(0, 0);
	
	//99.74% will fall in m+3*sigma  area.
	double norm_factor = 255.0f / (m + 6 * std);
	d.convertTo(d, CV_8U, norm_factor);

	for (size_t row = 0; row < d.rows; row++)
	{
		for (size_t col = 0; col < d.cols; col++)
		{
			if (d.at<uchar>(row,col) < 250)
			{
				continue;
			}
			d.at<uchar>(row, col) = m;
		}
	}	

	//down-sample for speed up
	Mat tmp_img;
	cv::pyrDown(d, tmp_img, Size(d.cols >> 1, d.rows >> 1));
	cv::pyrDown(tmp_img, d, Size(tmp_img.cols >> 1, tmp_img.rows >> 1));

	//binary  
	cv::threshold(tmp_img, bw, 0.0f, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
#ifdef  DEBUG_DETCT_BUBBLE
	imwrite("./differ_AG.tif", d);
	imwrite("./downsample_AG.tif", tmp_img);
	imwrite("./bw_AG.tif", bw);
#endif //  _DEBUG	
	return 0;
}

/*@brief: checkborder func remove bw's black borders assuming the edge is in image.
@bw: input binary image
@bw_cutborder: output image that have cutted borders.
*/
void DetectBubble::checkborder(const cv::Mat& bw, cv::Mat& bw_cutborder)
{
	//border:  |---0----|
	//         |        |
	//        1|        |3
	//         |---2----|
	const int border_width = std::min(bw.rows/2, bw.cols/2);
	int border[4] = { border_width,border_width, bw.rows - border_width, bw.cols - border_width };
	
	const int sum_th = 20 * 255;
	for (size_t y = 0; y < border_width; y++)
	{
		int sum = 0;
		for (size_t x = 0; x < bw.cols; x++)
		{
			sum += bw.at<unsigned char>(y, x);
		}
		if (sum > sum_th)
		{
			border[0] = y;
			break;
		}
	}

	for (size_t y = bw.rows - 1; y > bw.rows - 1 - border_width; y--)
	{
		int sum = 0;
		for (size_t x = 0; x < bw.cols; x++)
		{
			sum += bw.at<unsigned char>(y, x);
		}
		if (sum > sum_th)
		{
			border[2] = y;
			break;
		}
	}

	for (size_t x = 0; x < border_width; x++)
	{
		int sum = 0;
		for (size_t y = 0; y < bw.rows; y++)
		{
			sum += bw.at<unsigned char>(y, x);
		}
		if (sum > sum_th)
		{
			border[1] = x;
			break;
		}
	}
	for (size_t x = bw.cols - 1; x > bw.cols - 1 - border_width; x--)
	{
		int sum = 0;
		for (size_t y = 0; y < bw.rows; y++)
		{
			sum += bw.at<unsigned char>(y, x);
		}
		if (sum > sum_th)
		{
			border[3] = x;
			break;
		}
	}
	cv::Rect rect = cv::Rect(border[1], border[0], border[3] - border[1] + 1, border[2] - border[0] + 1);
	start.x = border[1], start.y = border[0];
	bw_cutborder = (cv::Mat(bw, rect)).clone();
	return;
}

void DetectBubble::findBlobs(const cv::Mat & img, const cv::Mat & binaryImage, std::vector<Center>& centers)
{
	using cv::Point;
	using cv::Point2d;
	using cv::Mat;
	using std::vector;
	centers.clear();
	vector < vector<Point> > contours;
	Mat tmpBinaryImage = binaryImage.clone();
	cv::findContours(tmpBinaryImage, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

#ifdef DEBUG_BLOB_DETECTOR
	Mat keypointsImage;
	cvtColor(binaryImage, keypointsImage, CV_GRAY2RGB);

	Mat contoursImage;
	cvtColor(binaryImage, contoursImage, CV_GRAY2RGB);
	imwrite("./RGBImage.tif", contoursImage);
	drawContours(contoursImage, contours, -1, cv::Scalar(0, 255, 0));
	imwrite("./contour.tif", contoursImage);			
#endif

	for (size_t contourIdx = 0; contourIdx < contours.size(); contourIdx++)
	{				
		Center center;
		center.confidence = 1;
		cv::Moments moms = cv::moments(Mat(contours[contourIdx]));
		if (this->params.filterByArea)
		{
			double area = moms.m00;
			if (area < this->params.minArea || area >= this->params.maxArea)
				continue;
		}

		if (this->params.filterByCircularity)
		{
			double area = moms.m00;
			double perimeter = cv::arcLength(Mat(contours[contourIdx]), true);
			double ratio = 4 * CV_PI * area / (perimeter * perimeter);
			if (ratio < this->params.minCircularity || ratio >= this->params.maxCircularity)
				continue;
		}

		if (this->params.filterByInertia)
		{
			double denominator = sqrt(pow(2 * moms.mu11, 2) + pow(moms.mu20 - moms.mu02, 2));
			const double eps = 1e-2;
			double ratio;
			if (denominator > eps)
			{
				double cosmin = (moms.mu20 - moms.mu02) / denominator;
				double sinmin = 2 * moms.mu11 / denominator;
				double cosmax = -cosmin;
				double sinmax = -sinmin;

				double imin = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmin - moms.mu11 * sinmin;
				double imax = 0.5 * (moms.mu20 + moms.mu02) - 0.5 * (moms.mu20 - moms.mu02) * cosmax - moms.mu11 * sinmax;
				ratio = imin / imax;
			}
			else
			{
				ratio = 1;
			}

			if (ratio < this->params.minInertiaRatio || ratio >= this->params.maxInertiaRatio)
				continue;

			center.confidence = ratio * ratio;
		}

		if (this->params.filterByConvexity)
		{
			vector < Point > hull;
			cv::convexHull(Mat(contours[contourIdx]), hull);
			double area = cv::contourArea(Mat(contours[contourIdx]));
			double hullArea = cv::contourArea(Mat(hull));
			double ratio = area / hullArea;
			if (ratio < this->params.minConvexity || ratio >= this->params.maxConvexity)
				continue;
		}
				
		center.location = Point2d(moms.m10 / moms.m00, moms.m01 / moms.m00);
		//cout << "center:" << center.location << endl;

		if (this->params.filterByColor)
		{
			if (center.location.y < 5 || center.location.x < 5 || center.location.y > binaryImage.rows - 5 || center.location.x > binaryImage.cols - 5)
			{
				continue;
			}
			
			
			int row = cvRound(center.location.y - 5 + start.y) * 4;
			int col = cvRound(center.location.x - 5 + start.x) * 4;
			auto limit = [](int x, int len) {x = x > 0 ? x : 0; x = x < len ? x : len - 1; return x; };
			// img is original image before pyrimid down-sampleing, its size = 4*binarysize
			// This line means to remove bright inpurity.
			if (img.at<unsigned short>(limit(row,img.rows), limit(col, img.cols)) > this->params.maxBlobColor)
				continue;
		}

		if (this->params.filterByRadius)
		{
			vector<double> dists;
			for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
			{
				Point2d pt = contours[contourIdx][pointIdx];
				dists.push_back(norm(center.location - pt));
			}
			std::sort(dists.begin(), dists.end());
			center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
			if (center.radius < this->params.minRadius)					
				continue;					
		}

#ifdef DEBUG_BLOB_DETECTOR				
		//compute blob radius
		{
			vector<double> dists;
			for (size_t pointIdx = 0; pointIdx < contours[contourIdx].size(); pointIdx++)
			{
				Point2d pt = contours[contourIdx][pointIdx];
				dists.push_back(norm(center.location - pt));
			}
			std::sort(dists.begin(), dists.end());
			center.radius = (dists[(dists.size() - 1) / 2] + dists[dists.size() / 2]) / 2.;
		}		
		cv::circle(keypointsImage, center.location, center.radius, cv::Scalar(0, 0, 255), 1);
		imwrite("./keypoints.tif", keypointsImage);
#endif
		centers.push_back(center);
	}
}


