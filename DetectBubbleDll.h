#ifndef DETECT_BUBBLE_DLL_H
#define DETECT_BUBBLE_DLL_H
#include <string>
#include <vector>

//__declspec(dllexport) int findBubble(char* p_A, char* p_C, char* p_G, char* p_T, const int rows, const int cols);
int findBubble(const std::vector<std::string> fns);

#endif
